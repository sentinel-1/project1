#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <cmath>
#include "defns.h"
using namespace std;

int convertToNum(string text){
    int k = 0;
    int n = text.size()-1;
    if(text[n] == 'M' || text[n] == 'm'){
        k = 6;
        text.pop_back();
        n--;
    }
    else if(text[n] == 'K' || text[n] == 'k'){
        k = 3;
        text.pop_back();
        n--;
    }
    n++;
    int x = -1;
    int sum = 0;
    for(int i = 0; i < n; i++){
        if(text[i] == '.'){
            sum = atoi(text.substr(0, i).c_str()) * pow(10, k);
            int dec = atoi(text.substr(i+1, text.size()-i-1).c_str()) * pow(10, k-(n-i-1));
            return sum+dec;
        }
    }
    sum = atoi(text.c_str()) * pow(10, k);
    return sum;
}

annual_storm *readFiles(int year){

	annual_storm *data = new annual_storm();
	data->year = year;

	string filename = "data/details-"+to_string(year)+string(".csv");
	ifstream details(filename);
	string buffer;
	int s = 0;
	getline(details, buffer);
	getline(details, buffer);
	while(details){
		getline(details, buffer);
		s++;
	}
	details.close();

	data->no_storms = s;
	data->storm_events = new storm_event[s];

	details.open(filename);
	getline(details, buffer);
	getline(details, buffer);
	int k = 0;
	while(details){
		int x = 0;
		int m = 0;
		int n = buffer.size();
		for(int i = 0; i <= n; i++){
			if(i == n || buffer[i] == ','){
				bool undef = false;
				if(i == m)
					undef = true;
				const char *text = buffer.substr(m, i-m).c_str();
				if(x == 0){
					data->storm_events[k].event_id = !undef? atoi(text):-1;
				}else if (x == 1){
					strcpy(data->storm_events[k].state,  !undef?text:"UNDEFINED");
				}
				else if (x == 2){
					data->storm_events[k].year = !undef? atoi(text):-1;
				}
				else if (x == 3){
					strcpy(data->storm_events[k].month_name, !undef?text:"UNDEFINED");
				}
				else if (x == 4){
					strcpy(data->storm_events[k].event_type,  !undef?text:"UNDEFINED");
				}
				else if (x == 5){
					data->storm_events[k].cz_type = !undef?text[0]:'U';
				}
				else if (x == 6){
					strcpy(data->storm_events[k].cz_name,  !undef?text:"UNDEFINED");
				}
				else if (x == 7){
					data->storm_events[k].injuries_direct = !undef? atoi(text):-1;
				}
				else if (x == 8){
					data->storm_events[k].injuries_indirect = !undef? atoi(text):-1;
				}
				else if (x == 9){
					data->storm_events[k].deaths_direct = !undef? atoi(text):-1;
				}
				else if (x == 10){
					data->storm_events[k].deaths_indirect = !undef? atoi(text):-1;
				}
				else if (x == 11){
					data->storm_events[k].damage_property = !undef? convertToNum(text):-1;
				}
				else if (x == 12){
					data->storm_events[k].damage_crops = !undef? convertToNum(text):-1;
				}
				x++;
				m = i+1;
			}
		}
		k++;
		getline(details, buffer);
	}
	details.close();

	filename = "data/fatalities-"+to_string(year)+string(".csv");
	ifstream fatalities(filename);
	int f = 0;
	getline(fatalities, buffer);
	getline(fatalities, buffer);
	while(fatalities){
		getline(fatalities, buffer);
		f++;
	}
	fatalities.close();

	data->no_fatalities = f;
	data->fatality_events = new fatality_event[f];
	
	fatalities.open(filename);
	getline(fatalities, buffer);
	getline(fatalities, buffer);
	k = 0;
	while(fatalities){
		int x = 0;
		int m = 0;
		int n = buffer.size();
		for(int i = 0; i <= n; i++){
			if(i == n || buffer[i] == ','){
				// cout << buffer.substr(m, i-m).c_str()<< endl;
				bool undef = false;
				if(i == m)
					undef = true;
				const char *text = buffer.substr(m, i-m).c_str();
				if(x == 0){
					data->fatality_events[k].event_id = !undef? atoi(text):-1;
				}else if (x == 1){
					data->fatality_events[k].fatality_type = !undef?text[0]:'U';
				}
				else if (x == 2){
					strcpy(data->fatality_events[k].fatality_date, !undef?buffer.substr(m, i-m).c_str():"UNDEFINED");
				}
				else if (x == 3){
					data->fatality_events[k].fatality_age = !undef? atoi(text):-1;
				}
				else if (x == 4){
					data->fatality_events[k].fatality_sex = !undef?text[0]:'U';
				}
				else if (x == 5){
					strcpy(data->fatality_events[k].fatality_location, !undef?text:"UNDEFINED");
				}
				x++;
				m = i+1;
			}
		}
		k++;
		getline(fatalities, buffer);
	}
	fatalities.close();
	return data;
}

void insertionSortDamage(damage arr[], int n)  
{  
    int i, j;  
	damage key;
    for (i = 1; i < n; i++) 
    {  
        key = arr[i];  
        j = i - 1;  
  
        /* Move elements of arr[0..i-1], that are  
        greater than key, to one position ahead  
        of their current position */
        while (j >= 0 && arr[j].damage_amount > key.damage_amount) 
        {  
            arr[j + 1] = arr[j];  
            j = j - 1;  
        }  
        arr[j + 1] = key;  
    }  
}  
void insertionSortFatality(deaths arr[], int n)  
{  
    int i, j;  
	deaths key;
    for (i = 1; i < n; i++) 
    {  
        key = arr[i];  
        j = i - 1;  
  
        /* Move elements of arr[0..i-1], that are  
        greater than key, to one position ahead  
        of their current position */
        while (j >= 0 && arr[j].total_deaths > key.total_deaths) 
        {  
            arr[j + 1] = arr[j];  
            j = j - 1;  
        }  
        arr[j + 1] = key;  
    }  
}  

void insertionSortSecondary(storm_event *arr[], int n)  
{  
    int i, j;  
	storm_event *key;
    for (i = 1; i < n; i++) 
    {  
        key = arr[i];  
        j = i - 1;  
  
        /* Move elements of arr[0..i-1], that are  
        greater than key, to one position ahead  
        of their current position */
        while (j >= 0 && ((arr[j]->year > key->year) || (arr[j]->year == key->year && arr[j]->event_id > key->event_id))) 
        {  
            arr[j + 1] = arr[j];  
            j = j - 1;  
        }  
        arr[j + 1] = key;  
    }  
} 

void printFatalityQuery(annual_storm **data, int n, storm_event **results, int m){
	int year = -99;
	for(int i = 0; i < m; i++){
		if(year != results[i]->year){
			year = results[i]->year;
			cout << '\t' << year << endl;
		}
		int x = 0;
		for(int j = 0; j < n; j++){
			if(data[j]->year == year){
				x = j;
				break;
			}
		}
		cout << "\t\tEvent ID: " << results[i]->event_id << endl;
		for(int j = 0; j < data[x]->no_fatalities; j++){
			if(data[x]->fatality_events[j].event_id == results[i]->event_id){
				cout << "\t\t\tFatality Type: " << data[x]->fatality_events[j].fatality_type << endl; 
				cout << "\t\t\tFatality Date: " << data[x]->fatality_events[j].fatality_date << endl; 
			}
		}
		cout << endl;
	}
}

void fatality_query(annual_storm **storms, int years, string pos, string sort){
	
	int n = 0;
	for(int i = 0; i < years; i++)
		n += storms[i]->no_storms;
	deaths *data = new deaths[n];
	int x = 0;
	for(int i = 0; i < years; i++){
		for(int j = 0; j < storms[i]->no_storms; j++){
			data[x].index = j;
			data[x].year = storms[i]->storm_events[j].year;
			data[x].total_deaths = storms[i]->storm_events[j].deaths_direct + storms[i]->storm_events[j].deaths_indirect;
			x++;
		}
	}
	if(sort == "insertion")
		insertionSortFatality(data, n);
	else
		// Replace with merge sort code
		insertionSortFatality(data, n);

	int k = 0;
	if (pos == "max")
		k = n-1;
	else if (pos == "min")
		k = 0;
	else
		k = atoi(pos.c_str());
	while(k > 0 && data[k-1].total_deaths == data[k].total_deaths)
		k--;
	int s = k;
	while(k < n-1 && data[k+1].total_deaths == data[k].total_deaths)
		k++;
	
	x = 0;
	int m = k-s+1;
	storm_event **results = new storm_event*[m];
	for(int i = s; i <= k; i++){
		int year = data[i].year;
		for(int j = 0; j < years; j++)
			if(year == storms[j]->year)
				results[x++] = &storms[j]->storm_events[data[i].index];
	}

	if(sort == "insertion")
		insertionSortSecondary(results, m);
	else
		// Replace with merge sort code
		insertionSortSecondary(results, m);

	printFatalityQuery(storms, n, results, m);
}


void printDamageQuery(storm_event **results, int n){

	int year = -99;
	for(int i = 0; i < n; i++){
		if(year != results[i]->year){
			year = results[i]->year;
			cout << '\t' << year << endl;
		}
		cout << "\t\tEvent ID: " << results[i]->event_id << endl;
		cout << "\t\tState: " << results[i]->state << endl;
		cout << "\t\tYear: " << results[i]->year << endl;
		cout << "\t\tMonth: " << results[i]->month_name << endl;
		cout << "\t\tEvent Type: " << results[i]->event_type << endl;
		cout << "\t\tCounty/Parish/Marine: " << results[i]->cz_type << endl;
		cout << "\t\tCounty/Parish/Marine Name: " << results[i]->cz_name << endl;
		cout << "\t\tInjuries Direct: " << results[i]->injuries_direct << endl;
		cout << "\t\tInjuries Indirect: " << results[i]->injuries_indirect << endl;
		cout << "\t\tDeaths Direct: " << results[i]->deaths_direct << endl;
		cout << "\t\tDeaths Indirect: " << results[i]->deaths_indirect << endl;
		cout << "\t\tDamage to Propery: $" << results[i]->damage_property << endl;
		cout << "\t\tDamage to Crops: $" << results[i]->damage_crops << endl;
		cout << endl;
	}

}

void damage_query(annual_storm **storms, int years, string pos, string type, string sort){

	int n = 0;
	for(int i = 0; i < years; i++)
		n += storms[i]->no_storms;
	damage *data = new damage[n];
	int x = 0;
	for(int i = 0; i < years; i++){
		for(int j = 0; j < storms[i]->no_storms; j++){
			data[x].index = j;
			data[x].year = storms[i]->storm_events[j].year;
			if(type == "damage_property")
				data[x].damage_amount = storms[i]->storm_events[j].damage_property;
			else
				data[x].damage_amount = storms[i]->storm_events[j].damage_crops;
			x++;
		}
	}

	if(sort == "insertion")
		insertionSortDamage(data, n);
	else
		// Replace with merge sort code
		insertionSortDamage(data, n);

	int k = 0;
	if (pos == "max")
		k = n-1;
	else if (pos == "min")
		k = 0;
	else
		k = atoi(pos.c_str());

	while(k > 0 && data[k-1].damage_amount == data[k].damage_amount)
		k--;
	int s = k;
	while(k < n-1 && data[k+1].damage_amount == data[k].damage_amount)
		k++;
	
	x = 0;
	int m = k-s+1;
	storm_event **results = new storm_event*[m];
	for(int i = s; i <= k; i++){
		int year = data[i].year;
		for(int j = 0; j < years; j++)
			if(year == storms[j]->year)
				results[x++] = &storms[j]->storm_events[data[i].index];
	}

	if(sort == "insertion")
		insertionSortSecondary(results, m);
	else
		// Replace with merge sort code
		insertionSortSecondary(results, m);

	printDamageQuery(results, m);
}


void query(annual_storm **data, int years, string query){

	cout << "Query: " << query << endl;

	int x = -1;
	string pos;
	string year;
	string type;
	string sort;
	int m = 0;
	for(int i = 0; i <= query.size(); i++){
		if(i == query.size() || query[i] == ' '){
			string text = query.substr(m, i-m);
			if (x == 0){
				pos = text;
			}
			else if (x == 1){
				year = text;
			}
			else if (x == 2){
				type = text;
			}
			else if(x == 3){
				sort = text;
			}
			x++;
			m = i+1;
		}
	}

	if (year != "all"){
		for(int i = 0; i < years; i++){
			if(data[i]->year == atoi(year.c_str())){
				data = data+i;
				break;
			}
		}
		years = 1;
	}

	if(type == "fatality"){
		fatality_query(data, years, pos, sort);
	}
	else{
		damage_query(data, years, pos, type, sort);
	}
}


int main(int argc, char *argv[]){
	
	int n = atoi(argv[1]);
	
	annual_storm **data = new annual_storm*[n];
	for(int i = 0; i < n; i++){
		data[i] = readFiles(atoi(argv[2+i]));
	}
	if(argc == n + 2){
		int m = 0;
		cin >> m;
		cin.ignore(10, '\n');
		string query_string;
		for(int i = 0; i < m; i++){
			getline(cin, query_string);
			query(data, n, query_string);
		}
	}
	else if(argc == n + 3){
		ifstream queries(argv[n+2]);
		string query_string;
		getline(queries, query_string);
		while(queries){
			query(data, n, query_string);
			getline(queries, query_string);
		}
	}
	return 0;
}
